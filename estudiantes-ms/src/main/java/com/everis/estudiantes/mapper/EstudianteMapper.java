package com.everis.estudiantes.mapper;

import com.everis.estudiantes.dto.estudiante.*;
import com.everis.estudiantes.model.Estudiante;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EstudianteMapper {

    @Mapping(source = "tipoDocumento", target = "document_type")
    @Mapping(source = "numeroDocumento", target = "document_number")
    @Mapping(source = "edad", target = "age")
    @Mapping(source = "universidad", target = "university")
    @Mapping(source = "estadoCivil", target = "civil_status")
    @Mapping(source = "fechaNacimiento", target = "birth_date")
    @Mapping(source = "direccion", target = "address")
    @Mapping(source = "genero", target = "gender")
    Estudiante mapResquestDtoToEntity(RequestDto requestDto);

    @Mapping(source = "id", target = "id")
    @Mapping(source = "requestDto.tipoDocumento", target = "document_type")
    @Mapping(source = "requestDto.numeroDocumento", target = "document_number")
    @Mapping(source = "requestDto.edad", target = "age")
    @Mapping(source = "requestDto.universidad", target = "university")
    @Mapping(source = "requestDto.estadoCivil", target = "civil_status")
    @Mapping(source = "requestDto.fechaNacimiento", target = "birth_date")
    @Mapping(source = "requestDto.direccion", target = "address")
    @Mapping(source = "requestDto.genero", target = "gender")
    Estudiante mapResponseUpdateDtoToEntity(RequestDto requestDto, Long id);

    @Mapping(source = "id", target = "codigo")
    @Mapping(source = "document_type", target = "tipoDocumento")
    @Mapping(source = "document_number", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseDto mapEntityToResponseDto(Estudiante estudiante);

    @Mapping(source = "document_type", target = "tipoDocumento")
    @Mapping(source = "document_number", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    ResponseGetByIdDto mapEntityToResponseGetById(Estudiante estudiante);

    @Mapping(source = "id", target = "codigo")
    @Mapping(source = "document_type", target = "tipoDocumento")
    @Mapping(source = "document_number", target = "numeroDocumento")
    @Mapping(source = "age", target = "edad")
    @Mapping(source = "university", target = "universidad")
    @Mapping(source = "civil_status", target = "estadoCivil")
    @Mapping(source = "birth_date", target = "fechaNacimiento")
    @Mapping(source = "address", target = "direccion")
    @Mapping(source = "gender", target = "genero")
    DefaultDto mapEntityToDefaultDto(Estudiante estudiante);

    List<DefaultDto> mapEntityListToDefaultDto(List<Estudiante> estudiantes);

}
