package com.everis.estudiantes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EstudiantesMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(EstudiantesMsApplication.class, args);
	}

}
