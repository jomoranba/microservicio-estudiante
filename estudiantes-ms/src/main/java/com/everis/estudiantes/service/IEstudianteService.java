package com.everis.estudiantes.service;

import com.everis.estudiantes.model.Estudiante;

import java.util.List;

public interface IEstudianteService {

    public List<Estudiante> getAllEstudiante();

    public Estudiante getByIdEstudiante(Long id);

    public Estudiante saveEstudinte(Estudiante estudiante);

    public void deleteEstudiante(Long id);

}
