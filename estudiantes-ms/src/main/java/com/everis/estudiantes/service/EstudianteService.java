package com.everis.estudiantes.service;

import com.everis.estudiantes.model.Estudiante;
import com.everis.estudiantes.repository.EstudianteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EstudianteService implements IEstudianteService{

    @Autowired
    private EstudianteRepository estudianteRepository;

    @Override
    public List<Estudiante> getAllEstudiante() {
        return estudianteRepository.findAll();
    }

    @Override
    public Estudiante getByIdEstudiante(Long id) {
        return estudianteRepository.findById(id).orElse(null);
    }

    @Override
    public Estudiante saveEstudinte(Estudiante estudiante) {
        return estudianteRepository.save(estudiante);
    }

    @Override
    public void deleteEstudiante(Long id) {
        estudianteRepository.deleteById(id);
    }
}
