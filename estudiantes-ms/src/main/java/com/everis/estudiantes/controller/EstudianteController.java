package com.everis.estudiantes.controller;

import com.everis.estudiantes.dto.estudiante.*;
import com.everis.estudiantes.mapper.EstudianteMapper;
import com.everis.estudiantes.service.EstudianteService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
public class EstudianteController {

    @Autowired
    private EstudianteMapper estudianteMapper;

    @Autowired
    private EstudianteService estudianteService;

    @GetMapping("/estudiantes")
    public List<DefaultDto> getAllEstudiante(){
        return estudianteMapper.mapEntityListToDefaultDto(estudianteService.getAllEstudiante());
    }

    @GetMapping("/estudiante/{id}")
    @ApiOperation(value = "Find student by id",
            notes = "Also returns a link to retrieve all students with rel - all-students")
    public ResponseGetByIdDto getEstudianteById(@PathVariable Long id){
        return estudianteMapper.mapEntityToResponseGetById(estudianteService.getByIdEstudiante(id));
    }

    @PostMapping("/estudiante")
    public ResponseDto saveEstudiante(@RequestBody RequestDto requestDto){
        return estudianteMapper.mapEntityToResponseDto(estudianteService.saveEstudinte(estudianteMapper.mapResquestDtoToEntity(requestDto)));
    }

    @PutMapping("/estudiante/{id}")
    public ResponseDto updateEstudiante(@PathVariable Long id, @RequestBody RequestDto requestDto){
        return estudianteMapper.mapEntityToResponseDto(estudianteService.saveEstudinte(estudianteMapper.mapResponseUpdateDtoToEntity(requestDto,id)));
    }

    @DeleteMapping("/estudiante/{id}")
    public void deleteEstudiante(@PathVariable Long id){
        estudianteService.deleteEstudiante(id);
    }

}
