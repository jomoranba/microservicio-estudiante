package com.everis.estudiantes.model;

import io.swagger.annotations.ApiModel;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
@Data
@ApiModel(description="All details about the student. ")
public class Estudiante {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String document_type;

    private String document_number;

    private String age;

    private String university;

    private String civil_status;

    private Date birth_date;

    private String address;

    private String gender;

}
