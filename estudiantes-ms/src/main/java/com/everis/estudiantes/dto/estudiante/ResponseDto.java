package com.everis.estudiantes.dto.estudiante;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Date;

@Data
public class ResponseDto {
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private Long codigo;

    public String tipoDocumento;

    public String numeroDocumento;

    public String edad;

    public String universidad;

    public String estadoCivil;

    public Date fechaNacimiento;

    public String direccion;

    public String genero;

}
