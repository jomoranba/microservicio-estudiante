package com.everis.estudiantes.dto.estudiante;

import java.util.Date;

public class DefaultDto {

    public Long codigo;

    public String tipoDocumento;

    public String numeroDocumento;

    public String edad;

    public String universidad;

    public String estadoCivil;

    public Date fechaNacimiento;

    public String direccion;

    public String genero;

}
