package com.everis.estudiantes.dto.estudiante;

import lombok.Data;

import java.util.Date;

@Data
public class ResponseGetByIdDto {

    public String tipoDocumento;

    public String numeroDocumento;

    public String edad;

    public String universidad;

    public String estadoCivil;

    public Date fechaNacimiento;

    public String direccion;

    public String genero;
}
