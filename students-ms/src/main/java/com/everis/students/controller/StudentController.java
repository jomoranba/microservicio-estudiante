package com.everis.students.controller;

import com.everis.students.model.Student;
import com.everis.students.repository.StudentRepository;
<<<<<<< Updated upstream
=======
import com.everis.students.service.StudentService;
import io.swagger.annotations.ApiOperation;
>>>>>>> Stashed changes
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

<<<<<<< Updated upstream
=======
import java.time.Duration;
import java.util.Collections;

>>>>>>> Stashed changes
@RestController
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping(value = "/example", produces = "text/event-stream")
    public Flux<Integer> all(){
        Flux<Integer> flux = Flux.range(1, 30)
                .delayElements(Duration.ofSeconds(1))
                .filter(n -> n % 2 == 0)
                .map(n -> n*2);
        return flux;
    }

    @GetMapping(value= "/students", produces = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Flux<Student> getStudent(){
        return studentService.findAllStudent();
    }

<<<<<<< Updated upstream
    @PostMapping("/student")
=======
    @GetMapping("/student/{id}")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "Find student by id",
            notes = "Also returns a link to retrieve all students with rel - all-students")
    public ResponseEntity<Mono<Student>> getStudentGetById(@PathVariable("id") Long id){
        Mono<Student> student = studentService.findById(id);
        HttpStatus status = (student != null) ? HttpStatus.OK : HttpStatus.NOT_FOUND;
        return new ResponseEntity<>(student, status);
    }

    @PostMapping(value = "/student", consumes = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
>>>>>>> Stashed changes
    public Mono<Student> saveStudent(@RequestBody Student student){
       return studentService.saveStudent(student);

    }

    @PutMapping(value = "/student/{id}", consumes = "application/json")
    @ResponseStatus(HttpStatus.OK)
    public Mono<Student> updateStudent(@PathVariable Long id, @RequestBody Student requestStudent){
        return studentService
                    .findById(id)
                    .map(student -> {
                        student.setDocument_type(requestStudent.getDocument_type());
                        student.setDocument_number(requestStudent.getDocument_number());
                        student.setAge(requestStudent.getAge());
                        student.setUniversity(requestStudent.getUniversity());
                        student.setCivil_status(requestStudent.getCivil_status());
                        student.setBirth_date(requestStudent.getBirth_date());
                        student.setAddress(requestStudent.getAddress());
                        student.setGender(requestStudent.getGender());
                        return student;
                    })
                    .flatMap(student -> studentService.saveStudent(student));
    }

    @DeleteMapping("/student/{id}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Mono<Void> deleteStudent(@PathVariable Long id){
        return studentService.deleteStudent(id);
    }
}
