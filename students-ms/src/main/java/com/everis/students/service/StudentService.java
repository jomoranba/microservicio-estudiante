package com.everis.students.service;

import com.everis.students.model.Student;
import com.everis.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class StudentService implements IStudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Override
    public Mono<Student> findById(Long id) {
        return studentRepository.findById(id);
    }

    @Override
    public Flux<Student> findAllStudent() {
        return studentRepository.findAll();
    }

    @Override
    public Mono<Student> saveStudent(Student student) {
        return studentRepository.save(student);
    }

    @Override
    public Mono<Void> deleteStudent(Long id) {
        return studentRepository.deleteById(id);
    }
}
