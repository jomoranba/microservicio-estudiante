package com.everis.students.service;

import com.everis.students.model.Student;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IStudentService {

    Mono<Student> findById(Long id);

    Flux<Student> findAllStudent();

    Mono<Student> saveStudent(Student student);

    Mono<Void> deleteStudent(Long id);

}
