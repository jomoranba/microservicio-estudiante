package com.everis.students.model;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
@Table("Student")
public class Student {

    @Id
    private Long id;

    private String document_type;

    private String document_number;

    private String age;

    private String university;

    private String civil_status;

    private Date birth_date;

    private String address;

    private String gender;

}
