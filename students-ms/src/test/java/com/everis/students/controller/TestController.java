package com.everis.students.controller;

import com.everis.students.model.Student;
import com.everis.students.repository.StudentRepository;
import com.everis.students.service.StudentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserter;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.Date;
import java.util.Locale;

@RunWith(SpringRunner.class)
@WebFluxTest(StudentController.class)
@Import(StudentService.class)
public class TestController {

    @Autowired
    WebTestClient webTestClient;

    @MockBean
    private StudentRepository studentRepository;

    @Test
    public void TestCreateStudent(){
        Student student = new Student();
        student.setId(1L);
        student.setDocument_type("DNI");
        student.setDocument_number("73448595");
        student.setAge("17");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setBirth_date(LocalDate.now());
        student.setAddress("dsada");
        student.setGender("Masculino");

        Mockito.when(studentRepository.save(student)).thenReturn(Mono.just(student));

        webTestClient.post()
                .uri("/student")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromValue(student))
                .exchange()
                .expectStatus().isCreated();

        Mockito.verify(studentRepository, Mockito.times(1)).save(student);
    }

    @Test
    public void TestStudentGetById(){
        Student student = new Student();
        student.setId(1L);
        student.setDocument_type("DNI");
        student.setDocument_number("73448595");
        student.setAge("17");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setBirth_date(LocalDate.now());
        student.setAddress("dsada");
        student.setGender("Masculino");

        Mockito.when(studentRepository.findById(1L)).thenReturn(Mono.just(student));

        webTestClient.get().uri("/student/{id}",1L)
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.document_type").isNotEmpty()
                .jsonPath("$.id").isEqualTo(1L)
                .jsonPath("$.document_type").isEqualTo("DNI")
                .jsonPath("$.document_number").isEqualTo("73448595")
                .jsonPath("$.age").isEqualTo("17")
                .jsonPath("$.university").isEqualTo("UPAO")
                .jsonPath("$.civil_status").isEqualTo("Soltero")
                .jsonPath("$.birth_date").isEqualTo("2021-06-02")
                .jsonPath("$.address").isEqualTo("dsada")
                .jsonPath("$.gender").isEqualTo("Masculino");

        Mockito.verify(studentRepository, Mockito.times(1)).findById(1L);

    }

    @Test
    public void TestStudentDelete(){
        Student student = new Student();
        student.setId(1L);
        student.setDocument_type("DNI");
        student.setDocument_number("73448595");
        student.setAge("17");
        student.setUniversity("UPAO");
        student.setCivil_status("Soltero");
        student.setBirth_date(LocalDate.now());
        student.setAddress("dsada");
        student.setGender("Masculino");

        Mockito.when(studentRepository.deleteById(student.getId())).thenReturn(Mono.empty());

        webTestClient.delete().uri("/student/{id}",student.getId()).accept(MediaType.APPLICATION_JSON)
                .exchange()
                .expectStatus().isAccepted();

        Mockito.verify(studentRepository, Mockito.times(1)).deleteById(student.getId());
    }

    /*@Test
    public void StudentUpdate(){

        Student studentOrigin = new Student();
        studentOrigin.setId(1L);
        studentOrigin.setDocument_type("DNI");
        studentOrigin.setDocument_number("73448595");
        studentOrigin.setAge("17");
        studentOrigin.setUniversity("UPAO");
        studentOrigin.setCivil_status("Soltero");
        studentOrigin.setBirth_date(LocalDate.now());
        studentOrigin.setAddress("dsada");
        studentOrigin.setGender("Masculino");

        Student studentUpdate = new Student();
        studentUpdate.setDocument_type("DNI");
        studentUpdate.setDocument_number("73448595");
        studentUpdate.setAge("20");
        studentUpdate.setUniversity("UPN");
        studentUpdate.setCivil_status("Soltero");
        studentUpdate.setBirth_date(LocalDate.now());
        studentUpdate.setAddress("xxxxxx");
        studentUpdate.setGender("Masculino");

        Mockito.when(studentRepository.save(studentUpdate)).thenReturn(Mono.just(studentOrigin));

        webTestClient.put().uri("/student/{id}", studentOrigin.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(studentUpdate), Student.class)
                .exchange()
                .expectStatus().isOk();

        *//*webTestClient.put().uri("/student/{id}", studentOrigin.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .body(Mono.just(studentUpdate),Student.class)
                .retrieve()
                .bodyToMono(Student.class);*//*

        Mockito.verify(studentRepository, Mockito.times(1)).save(studentUpdate);

    }*/
}
